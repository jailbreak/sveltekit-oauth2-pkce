# SvelteKit OAuth2 PKCE

Authentication hook for SvelteKit implementing OAuth2 authorization code flow with PKCE.

## Description

OAuth2 defines several flows. This library implements the authorization code flow, with the addition of a dynamically generated secret used on each token request, known as the [PKCE extension](https://oauth.net/2/pkce).

It is intended to be used from a SvelteKit application using client-side components. Therefore it can be used with [SvelteKit static adapter](https://github.com/sveltejs/kit/tree/master/packages/adapter-static).

Technically, it wraps the [js-pkce](https://github.com/bpedroza/js-pkce) library as a SvelteKit hook.

See also:

- <https://aaronparecki.com/oauth-2-simplified/>
- <https://github.com/aaronpk/pkce-vanilla-js>

## Install

```bash
npm install -D sveltekit-oauth2-pkce
```

## Usage

TODO

```js
import { useAuth } from "sveltekit-oauth2-pkce"
```
